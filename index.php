<?php
session_start();
$gender = array("Nam", "Nữ");
$department = array("MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");

?>
<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8">

    <!-- base style  -->
    <link rel="stylesheet" href="styles.css">

    <!-- bootstrap -->
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"> -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
    <title>All Student</title>
</head>

<body>
<body>
    <?php
        $conn = mysqli_connect("localhost", "root", "", "web_excersise");

        // Check connection
        if (mysqli_connect_errno())
        {
            echo "<div class='alert alert-danger' role='alert'>" . mysqli_connect_error() . "</div>";
        }

        $base_query = "SELECT * FROM student WHERE";

        if (!empty($_POST)) {
            $de = "";
            $keyword = "";
            if (isset($_POST['department'])) {
                $de = $_POST['department'];
            }

            if (isset($_POST['keyword'])) {
                $keyword = $_POST['keyword'];
            }
            
            if ($de) {
                $base_query = $base_query . " faculty = '$de' AND";
            }
           
            if ($keyword) {
                $base_query = $base_query . " (name LIKE '%$keyword%' OR address LIKE '%$keyword%') AND";
            }
            
            $base_query = $base_query . " 1";

            $all_student = mysqli_query($conn, $base_query);
        }
        else {

            $all_student = mysqli_query($conn,"SELECT * FROM student");
        }

        $student_count = mysqli_num_rows($all_student);

        mysqli_close($conn);
    ?>
    <div class="all-student">
        <div class="search-box">
            <form id="search-form" action="" method="post">
                <div class="input-box">
                <label for="department" class="text-label">Phân khoa</label>
                    <select name="department" id="department" class="select-field">
                        <option value="" disabled selected value>Chọn phân khoa</option>
                        <?php
                        foreach ($department as $key => $value) {
                            if ($_GET['department'] == $key) {
                                echo "<option value='$key' selected>$value</option>";
                            } else {
                                echo "<option value='$key'>$value</option>";
                            }
                        }
                        ?>
                    </select>
                </div>
                <div class="input-box">
                    <label for="" class="text-label">Từ khóa</label>
                    <?php
                        if (isset($_GET['keyword'])) {
                            $keyword = $_GET['keyword'];
                        } else {
                            $keyword = "";
                        }
                        echo "<input type='text' name='keyword' id='keyword' class='text-field' value='$keyword'>";
                    ?>
                </div>
                <div class="d-flex justify-content-center">
                    <div class="p-2">
                        <div class="btn">
                            <input class="btn-submit" type="submit" value="Tìm kiếm"></input>
                        </div>
                    </div>
                    <div class="p-2">
                        <div class="btn">
                            <input class="btn-submit" name="clear" type="button" onclick="clearForm()" value="Xóa"></input>
                        </div>
                    </div>  
                </div>
            </form>
        </div>
        <div class="student-count">
            <div class="student-found">
                <p><b>Số sinh viên tìm thấy: <?php echo $student_count ?></b></p>
            </div>
            <a href="create.php" class="btn-submit">Thêm</a>
        </div>
        <div class="student-list">
            <table class="student-table">
                <tr class="header">
                    <td style="width:10%"><b>No.</b></td>
                    <td style="width:40%"><b>Tên sinh viên</b></td>
                    <td style="width:40%"><b>Khoa</b></td>
                    <td style="text-align:center; width:10%"><b>Action</b></td>
                </tr>
                
                <?php
                    $i = 1;
                    while($row = mysqli_fetch_array($all_student))
                    {
                        echo '<tr class="student-item">';
                        echo '<td>' . $i . '</td>';
                        echo '<td>' . $row['name'] . '</td>';
                        echo '<td>' . $department[$row['faculty']] . '</td>';
                        echo '
                                <td class="action">
                                    <buton class="btn-action">Xóa</buton>
                                    <buton class="btn-action">Sửa</buton>
                                </td>
                            ';
                        $i++;
                    }

                ?>

            </table>
        </div>
    </div>
</body>

<script>
    function clearForm() {
        // document.getElementById("search-form").reset();
        document.getElementById("department").value = "";
        document.getElementById("keyword").value = "";
    }
</script>


</html>

